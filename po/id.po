# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Nick Schermer <nick@xfce.org>, 2019
# Kukuh Syafaat <syafaatkukuh@gmail.com>, 2020
# Triyan W. Nugroho <triyan.wn@gmail.com>, 2020
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-19 01:16+0200\n"
"PO-Revision-Date: 2019-01-14 12:47+0000\n"
"Last-Translator: Triyan W. Nugroho <triyan.wn@gmail.com>, 2020\n"
"Language-Team: Indonesian (https://app.transifex.com/xfce/teams/16840/id/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: id\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../panel-plugin/calculator.c:156
#, c-format
msgid "Calculator error: %s"
msgstr "Galat kalkulator: %s"

#: ../panel-plugin/calculator.c:233
msgid " Calc:"
msgstr " Calc:"

#: ../panel-plugin/calculator.c:421
msgid "Calculator Plugin"
msgstr "Plugin Kalkulator"

#. add the "Close" button
#: ../panel-plugin/calculator.c:424 ../panel-plugin/calculator.c:501
msgid "_Close"
msgstr "_Tutup"

#. Appearance
#: ../panel-plugin/calculator.c:443
msgid "Appearance"
msgstr "Penampilan"

#: ../panel-plugin/calculator.c:453
msgid "Width (in chars):"
msgstr "Lebar (dalam karakter):"

#. History
#: ../panel-plugin/calculator.c:466
msgid "History"
msgstr "Riwayat"

#: ../panel-plugin/calculator.c:476
msgid "Size:"
msgstr "Ukuran:"

#. Behavior
#: ../panel-plugin/calculator.c:487
msgid "Behavior"
msgstr "Perilaku"

#: ../panel-plugin/calculator.c:493
msgid "Do not move cursor after calculation"
msgstr "Jangan pindahkan kursor setelah menghitung"

#: ../panel-plugin/calculator.c:525
msgid "Calculator for Xfce panel"
msgstr "Kalkulator untuk panel Xfce"

#: ../panel-plugin/calculator.c:573
msgid "Trigonometrics use degrees"
msgstr "Trigonometri menggunakan derajat"

#: ../panel-plugin/calculator.c:576
msgid "Trigonometrics use radians"
msgstr "Trigonometri menggunakan radian"

#. Add checkbox to enable hexadecimal output
#: ../panel-plugin/calculator.c:596
msgid "Hexadecimal output"
msgstr "Keluaran heksadesimal"

#: ../panel-plugin/calculator.desktop.in.h:1
msgid "Calculator"
msgstr "Kalkulator"

#: ../panel-plugin/calculator.desktop.in.h:2
msgid "Calculator plugin for the Xfce panel"
msgstr "Plugin kalkulator untuk panel Xfce"
